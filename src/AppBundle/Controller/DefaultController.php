<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use MovingImage\Bundle\VMProComments\Entity\Comment;
use MovingImage\Bundle\VMProComments\Form\CommentForm;


class DefaultController extends Controller
{
    public function indexAction(string $videoId, Request $request)
    {
        $video = $this->get('vmpro_api.client')->getVideo($this->getParameter('vmpro_vm_id'), $videoId);

        $comment = new Comment();

        $comment->setVideoId($video->getId());

        $form = $this->createForm(CommentForm::class, $comment,
            [
                'action' => $this->generateUrl(
                    'vm_pro_comments_add',
                    [
                        'videoId' => $video->getId()
                    ]
                )
            ]
        );

        $comments = $this->getDoctrine()->getRepository(Comment::class)->findAllByVideoId($video->getId());

        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'video' => $video,
            'form' => $form->createView(),
            'comments' => $comments,
        ]);
    }
}
